package com.waming.spring.sensitive.web;

import com.alibaba.fastjson.JSON;
import com.hose.mall.sensitive.plugin.utils.JsonUtils;
import com.waming.spring.sensitive.persistence.enity.Address;
import com.waming.spring.sensitive.persistence.enity.UserDTO;
import com.waming.spring.sensitive.persistence.mapper.UserMapper;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Copyright (c) 合思商城研发团队 https://www.ekuaibao.com
 * @Author: wangmi wangmi@hosecloud.com
 * @Date: 2023/10/30 13:48
 * @Version 1.0
 */
@RestController
@RequestMapping("mall/user")
public class UserController {
    @Resource
    private UserMapper userMapper;

    @GetMapping("/insert")
    public Boolean insert(){
        UserDTO userDTO = new UserDTO();
        userDTO.setAge(20);
        userDTO.setEmail("238484@qq.com");
        userDTO.setIdcard("130722199102900995");
        userDTO.setIdcardSensitive("130722199102900995");
        userDTO.setUserName("李世民");
        userDTO.setUserNameSensitive("李世民");

        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("username","李世民");
        jsonMap.put("idcard","13072219991947585896");
        jsonMap.put("age",18);
        userDTO.setJsonStr(JsonUtils.parseMaptoJSONString(jsonMap));
        int result = userMapper.insert(userDTO);
        return Boolean.TRUE;
    }

    @GetMapping("/batch")
    public Boolean batch(){
        Long now=System.currentTimeMillis();
        List<UserDTO> userDTOS=new ArrayList<>();
        for (int i =0; i <10 ; i++) {
            UserDTO userDTO = new UserDTO();
            userDTO.setAge(i);
            userDTO.setEmail("hs"+i+"@qq.com");
            userDTO.setIdcard("130722199102"+i);
            userDTO.setUserName("李"+i+"民");
            userDTO.setUserNameSensitive(userDTO.getUserName());

            Map<String,Object> jsonMap = new HashMap<>();
            jsonMap.put("username","李"+i+"民");
            jsonMap.put("idcard","13072219991996"+i);
            jsonMap.put("age",18);
            userDTO.setJsonStr(JsonUtils.parseMaptoJSONString(jsonMap));
            userDTOS.add(userDTO);
            if(userDTOS.size()==50){
                int result = userMapper.insertBatch(userDTOS);
                userDTOS=new ArrayList<>();
            }
        }
        if(userDTOS.size()>0){
            int result = userMapper.insertBatch(userDTOS);
            System.out.println(result);
            System.out.println(JSON.toJSON(userDTOS));
        }
        return Boolean.TRUE;
    }

    @GetMapping("/update")
    public Boolean update(){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1);
        UserDTO userDTO2 = userMapper.findOne(userDTO);
        System.out.println(JSON.toJSON(userDTO2));

        userDTO2.setEmail("zhangsan@hosecloud.com");
        userDTO2.setIdcard("234422234343241");
        userDTO2.setIdcardSensitive("234422234343241");
        int result = userMapper.updateByCondition(userDTO2);
        return Boolean.TRUE;
    }

    @PostMapping("/updateByUser")
    public Boolean updateByUser(@RequestBody UserDTO userDTO){
        UserDTO u=userMapper.findByPk(userDTO.getId());
        if(u!=null){
            u.setUserName(userDTO.getUserName());
            u.setUserNameSensitive(u.getUserName());
            u.setAge(userDTO.getAge());
            u.setEmail(userDTO.getEmail());
            u.setIdcard(userDTO.getIdcard());
            u.setIdcardSensitive(userDTO.getIdcardSensitive());
            int result = userMapper.updateByCondition(u);
        }
        return Boolean.TRUE;
    }


    @GetMapping("/findAll")
    public List<UserDTO> find(){
        List<UserDTO> userDTOS=userMapper.findAll();
        for (UserDTO u:userDTOS) {
            Address address=new Address("100020","北京","北京市海淀区学院路39号唯实大厦10层");
            u.setAddress(address);
        }
        return userDTOS;
    }

    @PostMapping("/page")
    public List<UserDTO> page(@RequestBody UserDTO userDTO){
        Map<String, Object> paramter=new HashMap<>();
        paramter.put("item",userDTO);
        paramter.put("startPoin",0);
        paramter.put("endPoin",10);
        List<UserDTO> userDTOS=userMapper.selectByModel(paramter);
        for (UserDTO u:userDTOS) {
            Address address=new Address("100020","北京","北京市海淀区学院路");
            u.setAddress(address);
        }
        return userDTOS;
    }



    @GetMapping("/find2")
    public List<UserDTO> find2(){
        List<UserDTO> userDTOS=userMapper.findAll2();
        for (UserDTO u:userDTOS) {
            Address address=new Address("100020","北京","北京市海淀区学院路");
            u.setAddress(address);
        }
        return userDTOS;
    }

    @GetMapping("/find/id")
    public UserDTO findId(@RequestParam("id") Integer id){
        UserDTO userDTO=userMapper.findByPk(id);
        if(userDTO!=null){
            Address address=new Address("100020","北京","北京市海淀区学院路");
            userDTO.setAddress(address);
        }
        return userDTO;
    }
}
