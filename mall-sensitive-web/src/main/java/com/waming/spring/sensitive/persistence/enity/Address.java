package com.waming.spring.sensitive.persistence.enity;

import com.hose.mall.sensitive.plugin.annotation.SensitiveField;
import com.hose.mall.sensitive.plugin.strategy.SensitiveType;
import java.io.Serializable;

/**
 * @Copyright (c) 合思商城研发团队 https://www.ekuaibao.com
 * @Author: wangmi wangmi@hosecloud.com
 * @Date: 2023/10/30 18:12
 * @Version 1.0
 */
public class Address implements Serializable {
    private String post;
    private String city;
    @SensitiveField(SensitiveType.ADDRESS)
    private String street;

    public String getPost() {
        return post;
    }
    public void setPost(String post) {
        this.post = post;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }


    @Override
    public String toString() {
        return "Address{" +
                "post='" + post + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                '}';
    }

    public Address(String post, String city, String street) {
        this.post = post;
        this.city = city;
        this.street = street;
    }

    public Address() {
        super();
    }
}
