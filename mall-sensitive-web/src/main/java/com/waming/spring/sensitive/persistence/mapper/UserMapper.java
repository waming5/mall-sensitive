package com.waming.spring.sensitive.persistence.mapper;

import com.waming.spring.sensitive.persistence.enity.UserDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {
    int insert(UserDTO userDTO);
    int insertBatch(@Param("list") List<UserDTO> list);
    int insert2(@Param("userName") String userName,@Param("idcard") String idcard);
    List<UserDTO> findAll();
    List<UserDTO> selectByModel(Map<String, Object> paramter);


    List<UserDTO> findAll2();
    UserDTO findOne(UserDTO userDTO);
    /**
     * 测试 通过加密字段查询的情况
     * @param userDTO ;
     * @return ;
     */
    List<UserDTO> findByCondition(UserDTO userDTO);

    /**
     * 测试更新时带条件字段，update的字段里有需要加密的字段，where条件里也有需要加密的
     * @param userDTO userDto
     * @return return
     */
    int updateByCondition(UserDTO userDTO);

    UserDTO findByPk(Integer id);
}
