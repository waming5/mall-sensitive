package com.waming.spring.sensitive.config;

import com.hose.mall.sensitive.plugin.encrypt.AesSupport;
import com.hose.mall.sensitive.plugin.encrypt.Encrypt;
import com.hose.mall.sensitive.plugin.handler.AnnotationHandler;
import com.hose.mall.sensitive.plugin.mybatis.interceptor.DecryptInterceptor;
import com.hose.mall.sensitive.plugin.mybatis.interceptor.EncryptWriteInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;


/**
 * 插件配置
 */
@Configuration
public class EncryptPluginConfig {
    @Bean
    Encrypt encryptor() throws Exception{ ;
       return new AesSupport("3245577f29b17d6787782f35998c4a79");
    }

    @Bean
    ConfigurationCustomizer configurationCustomizer() throws Exception{
        AnnotationHandler annotationHandler=new AnnotationHandler();
        DecryptInterceptor decryptReadInterceptor = new DecryptInterceptor(annotationHandler,encryptor());
        EncryptWriteInterceptor encryptWriteInterceptor = new EncryptWriteInterceptor(annotationHandler,encryptor());
        return (configuration) -> {
            configuration.addInterceptor(decryptReadInterceptor);
            configuration.addInterceptor(encryptWriteInterceptor);
        };
    }
}
