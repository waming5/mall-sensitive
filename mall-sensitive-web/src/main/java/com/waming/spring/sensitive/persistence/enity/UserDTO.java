package com.waming.spring.sensitive.persistence.enity;

import com.hose.mall.sensitive.plugin.annotation.EncryptEnabled;
import com.hose.mall.sensitive.plugin.annotation.EncryptField;
import com.hose.mall.sensitive.plugin.annotation.SensitiveBinded;
import com.hose.mall.sensitive.plugin.annotation.SensitiveField;
import com.hose.mall.sensitive.plugin.strategy.SensitiveType;

@EncryptEnabled
public class UserDTO {
    private static final int vid=33;
    private Integer id;
    /**
     * 用户名
     */
    @EncryptField
    @SensitiveField(SensitiveType.CHINESE_NAME)
    private String userName;
    /**
     * 脱敏的用户名
     */
    @SensitiveField(SensitiveType.CHINESE_NAME)
    private String userNameSensitive;
    /**
     * 值的赋值不从数据库取，而是从userName字段获得。
     */
    @SensitiveBinded(bindField = "userName",value = SensitiveType.CHINESE_NAME)
    private String userNameOnlyDTO;
    /**
     * 身份证号
     */
    @EncryptField
    @SensitiveField(SensitiveType.ID_CARD)
    private String idcard;
    /**
     * 脱敏的身份证号
     */
    @SensitiveField(SensitiveType.ID_CARD)
    private String idcardSensitive;

    /**
     * 一个json串，需要脱敏
     * SensitiveJSONField标记json中需要脱敏的字段
     */
    private String jsonStr;

    private int age;

    @SensitiveField(SensitiveType.EMAIL)
    @EncryptField
    private String email;

    private Address address;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNameSensitive() {
        return userNameSensitive;
    }
    public void setUserNameSensitive(String userNameSensitive) {
        this.userNameSensitive = userNameSensitive;
    }

    public String getUserNameOnlyDTO() {
        return userNameOnlyDTO;
    }
    public void setUserNameOnlyDTO(String userNameOnlyDTO) {
        this.userNameOnlyDTO = userNameOnlyDTO;
    }

    public String getIdcard() {
        return idcard;
    }
    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getIdcardSensitive() {
        return idcardSensitive;
    }
    public void setIdcardSensitive(String idcardSensitive) {
        this.idcardSensitive = idcardSensitive;
    }

    public String getJsonStr() {
        return jsonStr;
    }
    public void setJsonStr(String jsonStr) {
        this.jsonStr = jsonStr;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
}
