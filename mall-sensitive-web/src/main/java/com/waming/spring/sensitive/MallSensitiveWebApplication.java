package com.waming.spring.sensitive;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.waming.spring.sensitive"})
@MapperScan(basePackages = "com.waming.spring.sensitive.persistence.mapper")
public class MallSensitiveWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(MallSensitiveWebApplication.class, args);
	}

}
