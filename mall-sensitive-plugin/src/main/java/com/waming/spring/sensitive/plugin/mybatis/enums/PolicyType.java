package com.waming.spring.sensitive.plugin.mybatis.enums;

public enum PolicyType {
    /***加密***/
    ENCRYPT,
    /***脱敏***/
    SENSITIVE,
    /***别名脱敏***/
    ALIASBIND,
    /***所有属性***/
    FIELD,
}
