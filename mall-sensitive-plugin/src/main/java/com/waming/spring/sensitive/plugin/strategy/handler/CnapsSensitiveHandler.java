package com.waming.spring.sensitive.plugin.strategy.handler;

import com.waming.spring.sensitive.plugin.strategy.SensitiveType;
import com.waming.spring.sensitive.plugin.strategy.SensitiveTypeHandler;

/**
 * 公司开户银行联号
 * 前四位明文，后面脱敏
 * @author chenhaiyang
 */
public class CnapsSensitiveHandler implements SensitiveTypeHandler {
    @Override
    public SensitiveType getSensitiveType() {
        return SensitiveType.CNAPS_CODE;
    }
    public final static String CNAPS = "(?<=\\w{4})\\w(?=\\w{4})";

    @Override
    public String handle(Object src) {
        if(src==null){
            return null;
        }
        String snapCard =src.toString();
        return snapCard.replaceAll(CNAPS, "*");
    }
}
