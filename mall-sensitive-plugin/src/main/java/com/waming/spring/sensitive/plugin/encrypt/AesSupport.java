package com.waming.spring.sensitive.plugin.encrypt;
import com.waming.spring.sensitive.plugin.strategy.SensitiveType;
import com.waming.spring.sensitive.plugin.strategy.SensitiveTypeHandler;
import com.waming.spring.sensitive.plugin.strategy.SensitiveTypeRegisty;
import com.waming.spring.sensitive.plugin.utils.Hex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * 数据脱敏用到的AES加解密类
 */
public class AesSupport implements Encrypt {
    private static Logger log= LogManager.getLogger(AesSupport.class);
    private static final String KEY_ALGORITHM = "AES";
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
    private static final String CHARSET_NAME="UTF-8";
    private String salt;
    private SecretKeySpec secretKeySpec;
    private SensitiveTypeHandler sensitiveTypeHandler = SensitiveTypeRegisty.get(SensitiveType.DEFAUL);

    public AesSupport(String salt) throws NoSuchAlgorithmException {
        if(StringUtils.isEmpty(salt)){
            throw new IllegalArgumentException("password should not be null!");
        }
        this.salt = salt;
        this.secretKeySpec = getSecretKey(salt);
    }

    @Override
    public String encrypt(String value){
        try{
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            byte[] content = value.getBytes(CHARSET_NAME);
            byte[] encryptData = cipher.doFinal(content);
            return Hex.bytesToHexString(encryptData);
        }catch (Exception e){
            log.error("AES加密时出现问题，密钥为：{}",sensitiveTypeHandler.handle(salt));
            throw new IllegalStateException("AES加密时出现问题"+e.getMessage(),e);
        }
    }

    @Override
    public String decrypt(String value){
        if (StringUtils.isEmpty(value)) {
            return "";
        }
        try {
            byte[] encryptData = Hex.hexStringToBytes(value);
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] content = cipher.doFinal(encryptData);
            return new String(content,CHARSET_NAME);
        }catch (Exception e){
            log.error("AES解密时出现问题，密钥为:{},密文为：{}",sensitiveTypeHandler.handle(salt),value);
            throw new IllegalStateException("AES解密时出现问题"+e.getMessage(),e);
        }
    }


    private static SecretKeySpec getSecretKey(final String salt) throws NoSuchAlgorithmException{
        //返回生成指定算法密钥生成器的 KeyGenerator 对象
        KeyGenerator kg = KeyGenerator.getInstance(KEY_ALGORITHM);
        //AES 要求密钥长度为 128
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(salt.getBytes());
        kg.init(128, random);
        //生成一个密钥
        SecretKey secretKey = kg.generateKey();
        // 转换为AES专用密钥
        return new SecretKeySpec(secretKey.getEncoded(), KEY_ALGORITHM);
    }
}
