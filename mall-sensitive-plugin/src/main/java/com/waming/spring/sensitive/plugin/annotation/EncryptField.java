package com.waming.spring.sensitive.plugin.annotation;
import java.lang.annotation.*;
/**
 * 注解加解密属性
 * 写时对数据进行加密，读取求时进行解密
 */
@Target({ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface EncryptField {
}
