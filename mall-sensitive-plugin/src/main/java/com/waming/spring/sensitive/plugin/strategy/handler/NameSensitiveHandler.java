package com.waming.spring.sensitive.plugin.strategy.handler;


import com.waming.spring.sensitive.plugin.strategy.SensitiveType;
import com.waming.spring.sensitive.plugin.strategy.SensitiveTypeHandler;

/**
 * 姓名脱敏的解析类
 * 中文姓名只显示第一个汉字，其他隐藏为2个星号
 * 例子：李**
 * 张三丰 ：张**
 * @author ;
 */
public class NameSensitiveHandler implements SensitiveTypeHandler {
    @Override
    public SensitiveType getSensitiveType() {
        return SensitiveType.CHINESE_NAME;
    }
    
    @Override
    public String handle(Object src) {
        if (src==null) {
            return "";
        }
        String fullName = src.toString();
        String name = StringUtils.right(fullName, 1);
        return StringUtils.leftPad(name, StringUtils.length(fullName), "*");
    }
}
