package com.waming.spring.sensitive.plugin.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.waming.spring.sensitive.plugin.json.serializer.SensitiveHandler;
import com.waming.spring.sensitive.plugin.strategy.SensitiveType;

import java.lang.annotation.*;
/**
 * 敏感
 * 数据脱敏注解
 * @author
 * @date 2023/08/09
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@JacksonAnnotationsInside
@JsonSerialize(using = SensitiveHandler.class)
public @interface SensitiveField {
    /**
     * 脱敏类型
     * 不同的脱敏类型置换*的方式不同
     * @return SensitiveType
     */
    SensitiveType value();
}
