package com.waming.spring.sensitive.plugin.annotation;

import java.lang.annotation.*;

/**
 * 用于说明是否支持加解密
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface EncryptEnabled {
    /**
     * 是否开启加解密和脱敏模式
     * @return SensitiveEncryptEnabled
     */
    boolean value() default true;
}
