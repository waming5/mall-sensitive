package com.waming.spring.sensitive.plugin.mybatis.interceptor;

import com.waming.spring.sensitive.plugin.annotation.SensitiveBinded;
import com.waming.spring.sensitive.plugin.annotation.EncryptEnabled;
import com.waming.spring.sensitive.plugin.encrypt.Encrypt;
import com.waming.spring.sensitive.plugin.handler.AnnotationHandler;
import com.waming.spring.sensitive.plugin.mybatis.enums.PolicyType;
import com.waming.spring.sensitive.plugin.strategy.SensitiveType;
import com.waming.spring.sensitive.plugin.strategy.SensitiveTypeRegisty;
import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.lang.reflect.Field;
import java.util.*;

/**
 * 对响应结果进行拦截处理,对需要解密的字段进行解密
 * SQL样例：
 *  1. UPDATE tbl SET x=?, y =
 */
@Intercepts({
        @Signature(type = ResultSetHandler.class, method = "handleResultSets", args = {java.sql.Statement.class})
})
public class DecryptInterceptor implements Interceptor {
    private static final Logger LOG= LogManager.getLogger(DecryptInterceptor.class);
    private static final String MAPPED_STATEMENT="mappedStatement";
    private Encrypt encrypt;
    private final AnnotationHandler annotationCacheHandler;

    public DecryptInterceptor(AnnotationHandler annotationCache, Encrypt encrypt) {
        Objects.requireNonNull(encrypt,"encrypt should not be null!");
        this.encrypt = encrypt;
        this.annotationCacheHandler=annotationCache;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        final List<Object> results = (List<Object>)invocation.proceed();
        if (results.isEmpty()) {
            return results;
        }
        final ResultSetHandler statementHandler = annotationCacheHandler.realTarget(invocation.getTarget());
        final MetaObject metaObject = SystemMetaObject.forObject(statementHandler);
        final MappedStatement mappedStatement = (MappedStatement)metaObject.getValue(MAPPED_STATEMENT);
        final ResultMap resultMap = mappedStatement.getResultMaps().isEmpty() ? null : mappedStatement.getResultMaps().get(0);
        if(resultMap==null){
            return results;
        }
        Object result0 = results.get(0);
        EncryptEnabled encryptEnabled = result0.getClass().getAnnotation(EncryptEnabled.class);
        if(encryptEnabled == null || !encryptEnabled.value()){
            return results;
        }
        Class<?> clazz = resultMap.getType();
        annotationCacheHandler.parseClass(clazz);
        //解密
        final Set<Field> encryptFieldMap = getSensitiveByResultMap(clazz, PolicyType.ENCRYPT);
        //脱敏属性绑定别名
        final Set<Field> sensitiveBindedMap = getSensitiveByResultMap(clazz,PolicyType.ALIASBIND);
        if (sensitiveBindedMap.isEmpty() && encryptFieldMap.isEmpty()) {
            return results;
        }
        resulstDecode(results, mappedStatement, encryptFieldMap, sensitiveBindedMap, encrypt);
        return results;
    }

    private static void resulstDecode(List<Object> results, MappedStatement mappedStatement,Set<Field> encryptFieldMap,Set<Field> sensitiveBindedMap, Encrypt encrypt) {
        for (Object obj: results) {
            final MetaObject objMetaObject = mappedStatement.getConfiguration().newMetaObject(obj);
            for (Field field : encryptFieldMap) {
                String property = field.getName();
                String value = (String) objMetaObject.getValue(property);
                if (value != null) {
                    try {
                        String decryptValue = encrypt.decrypt(value);
                        objMetaObject.setValue(property, decryptValue);
                    }catch (Exception e){
                        LOG.error("DecryptReadInterceptor resulstDecode intercept error property:"+property+" value:"+value,e);
                    }
                }
            }
            for (Field field:sensitiveBindedMap) {
                SensitiveBinded sensitiveBinded =field.getAnnotation(SensitiveBinded.class);
                String bindPropety = sensitiveBinded.bindField();
                SensitiveType sensitiveType = sensitiveBinded.value();
                String value = (String) objMetaObject.getValue(bindPropety);
                try {
                    String resultValue =  SensitiveTypeRegisty.get(sensitiveType).handle(value);
                    objMetaObject.setValue(field.getName(),resultValue);
                }catch (Exception e){
                    LOG.error("DecryptReadInterceptor resulstDecode intercept error property:"+field.getName()+" value:"+value,e);
                }
            }
        }
    }

    private Set<Field> getSensitiveByResultMap(Class<?> clazz,PolicyType policyType) {
        if (clazz == null) {
            return new HashSet<Field>(16);
        }
        return  annotationCacheHandler.getCahceFields(clazz,policyType);
    }

    @Override
    public Object plugin(Object o) {
        return Plugin.wrap(o, this);
    }

    @Override
    public void setProperties(Properties properties) {
    }
}
