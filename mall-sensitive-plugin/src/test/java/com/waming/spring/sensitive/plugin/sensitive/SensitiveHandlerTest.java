package com.waming.spring.sensitive.plugin.sensitive;

import com.waming.spring.sensitive.plugin.strategy.handler.*;
import org.junit.jupiter.api.Test;
public class SensitiveHandlerTest {

    @Test
    public void test(){
        MobilePhoneSensitiveHandler mobilePhoneSensitiveHandler = new MobilePhoneSensitiveHandler();
        String result = mobilePhoneSensitiveHandler.handle("18233583070");
        System.out.println(result);
    }

    @Test
    public void test2(){
        NameSensitiveHandler nameSensitiveHandler = new NameSensitiveHandler();
        String result = nameSensitiveHandler.handle("王秘");
        System.out.println(result);
    }

    @Test
    public void test3(){
        EmailSensitiveHandler nameSensitiveHandler = new EmailSensitiveHandler();
        String result = nameSensitiveHandler.handle("testemail@qq.com");
        System.out.println(result);
    }
    @Test
    public void test4(){
        AddressSensitiveHandler nameSensitiveHandler = new AddressSensitiveHandler();
        String result =nameSensitiveHandler.handle("北京市海淀区学院路39号唯实大厦10层");
        System.out.println(result);
    }

    @Test
    public void test5(){
        BandCardSensitiveHandler nameSensitiveHandler = new BandCardSensitiveHandler();
        String result = nameSensitiveHandler.handle("622000441341343");
        System.out.println(result);
    }
    @Test
    public void test6(){
        IDCardSensitiveHandler nameSensitiveHandler = new IDCardSensitiveHandler();
        String result = nameSensitiveHandler.handle("XDE038990");
        System.out.println(result);
    }

    @Test
    public void test7(){
        CnapsSensitiveHandler nameSensitiveHandler = new CnapsSensitiveHandler();
        String result = nameSensitiveHandler.handle("13072219910232323");
        System.out.println(result);
    }
    @Test
    public void test8(){
        FixedPhoneSensitiveHandler nameSensitiveHandler = new FixedPhoneSensitiveHandler();
        String result = nameSensitiveHandler.handle("86-10-66778899");
        System.out.println(result);
    }

    @Test
    public void test9(){
        PaySignNoSensitiveHandler nameSensitiveHandler = new PaySignNoSensitiveHandler();
        String result = nameSensitiveHandler.handle("19031317273364059018");
        System.out.println(result);
    }
}
